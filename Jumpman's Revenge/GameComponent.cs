﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;
using MySql.Data.MySqlClient;

namespace Jumpman_s_Revenge
{
    class GameComponent :DrawableGameComponent
    {
        public Object _levelLock = new Object();
        public Level level;
        public int TileW;
        public int TileH;
        public int StatusBarH;
        public Player[] Players = new Player[4];
        public byte PlayerID;
        public Texture2D PlayerTexture;
        public List<Control> Controls = new List<Control>();
        public Control GetControlByName(string name)
        {
            for (int i = 0; i < Controls.Count; i++)
                if (Controls[i].Name == name)
                    return Controls[i];
            return null;
        }


        public UdpClient listenClient;
        public Thread listenThread;

        public bool host;
        public Texture2D background;
        public DateTime lastPing = DateTime.Now;
        public Network network;
        public Random random;
        bool ShowCursor = false;

        public GameComponent(Game game)
            : base(game)
        {
            TileW = 48;
            TileH = 48;
            StatusBarH = 48;
            network = new Network(this);
            random = new Random();
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                try
                {
                    listenThread.Abort();
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }

                byte[] dcPkg = new byte[2];
                dcPkg[0] = (byte)PacketType.Disconnect;
                dcPkg[1] = PlayerID;
                if (host)
                {
                    for (byte i = 0; i < Players.Length; i++)
                        if (i != PlayerID && Players[i] != null)
                        {
                            try
                            {
                                listenClient.Send(dcPkg, dcPkg.Length, Players[i].clientIPE);
                            }
                            catch {  }
                        }
                    // -------------- remove from server list --------------------------
                    string connectionString = "SERVER=84.255.249.171;PORT=3306;DATABASE=jumpman;UID=jumpman;PASSWORD=jumpmanpass;";
                    MySqlConnection db = new MySqlConnection(connectionString);
                    try
                    {
                        db.Open();
                        MySqlCommand command = new MySqlCommand("DELETE FROM Servers WHERE IP IN (SELECT SUBSTRING_INDEX(host,':',1) AS 'IP' FROM information_schema.processlist WHERE ID=connection_id())", db);
                        command.ExecuteNonQuery();
                        db.Close();
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine(ex.ToString());
                    }
                }
                else
                {
                    try
                    {
                        listenClient.Send(dcPkg, dcPkg.Length);
                    }
                    catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                }
                try
                {
                    listenClient.Close();
                }
                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
            }
            base.Dispose(disposing);
        }
        public void Listener()
        {
                while (Thread.CurrentThread.IsAlive)
                {
                    IPEndPoint eRemote;
                    if (host)
                        eRemote = new IPEndPoint(IPAddress.Any, 15000);
                    else
                        eRemote = (IPEndPoint)listenClient.Client.RemoteEndPoint;
                    byte[] data = new byte[0];
                    try
                    {
                        data = listenClient.Receive(ref eRemote);
                    }
                    catch (ThreadAbortException ex)
                    {
                        break;
                    }
                    catch (SocketException ex)
                    {
                        Console.WriteLine(ex.ToString());
                        if (!host)
                        {
                            Abort();
                            break;
                        }
                    }
                    if (data.Length != 0)
                    {
                        PacketType type = (PacketType)data[0];
                        if (type == PacketType.Connecting && host)
                        {
                            Console.WriteLine("{0} connected", eRemote);
                            List<byte> sendData = new List<byte>();
                            byte newPlyID = 255;
                            for (byte i = 0; i < Players.Length; i++)
                            {
                                if (Players[i] == null)
                                {
                                    newPlyID = i;
                                    Players[newPlyID] = new Player(newPlyID, "", this);
                                    Players[newPlyID].clientIPE = eRemote;
                                    sendData.Add((byte)PacketType.Connecting);
                                    sendData.Add(newPlyID);
                                    sendData.AddRange(UTF8Encoding.UTF8.GetBytes(level.FileName));
                                    sendData.Add(0);
                                    break;
                                }
                            }
                            try
                            {
                                listenClient.Send(sendData.ToArray(), sendData.Count, eRemote); // send connecting ACK with player's index and level name
                            }
                            catch (Exception ex)
                            {
                                if (newPlyID != 255)
                                {
                                    Players[newPlyID] = null;
                                    newPlyID = 255;
                                }
                            }
                            if (newPlyID != 255)
                            {
                                for (byte i = 0; i < Players.Length; i++)
                                {
                                    if (i != newPlyID && Players[i] != null)
                                    {
                                        sendData = new List<byte>();
                                        sendData.Add((byte)PacketType.PlayerInfo);
                                        sendData.Add(i);
                                        sendData.AddRange(UTF8Encoding.UTF8.GetBytes(Players[i].Name));
                                        sendData.Add(0);
                                        sendData.Add(Players[i].Type);
                                        sendData.Add(Players[i].color.R);
                                        sendData.Add(Players[i].color.G);
                                        sendData.Add(Players[i].color.B);
                                        sendData.Add(Players[i].Lives);
                                        try
                                        {
                                            listenClient.Send(sendData.ToArray(), sendData.Count, eRemote);
                                        }
                                        catch
                                        {
                                        }
                                    }
                                }
                            }
                        }
                        else if (type == PacketType.PlayerInfo)
                        {
                            byte pID = data[1];       
                            int len = 0;
                            string name = Main.GetString(ref data, 2, out len);
                            byte index = (byte)(2 + len);
                            byte plyType = data[index++];
                            if (Players[pID] == null)
                                Players[pID] = new Player(pID, name, this);
                            else
                                Players[pID].Name = name;
                            Players[pID].Type = plyType;
                            Players[pID].color = new Color(data[index++], data[index++], data[index++]);
                            Players[pID].Lives = data[index++];
                           // Console.WriteLine("PlayerInfo: pID:{0} name:{1}, Players[pID].name:{2}", pID, name, Players[pID].Name);
                            if (host)
                            {
                                for (byte i = 0; i < Players.Length; i++)
                                    if (i != pID && i != PlayerID && Players[i] != null)
                                    {
                                        try
                                        {
                                            listenClient.Send(data, data.Length, Players[i].clientIPE);
                                        }
                                        catch (Exception ex) { Console.WriteLine(ex.ToString()); } 
                                    }
                            }
                        }
                        else if (type == PacketType.PlayerMove)
                        {
                            byte pID = data[1];
                            var player = Players[pID];
                            if (player != null)
                            {
                                UInt16 index = BitConverter.ToUInt16(data, 2);
                                if (index > player.moveIndex)
                                {
                                    player.moveIndex = index;
                                    player.moveFlags = (MoveFlags)data[4];
                                    player.Acceleration.X = BitConverter.ToSingle(data, 5);
                                    player.Acceleration.Y = BitConverter.ToSingle(data, 9);
                                    player.Velocity.X = BitConverter.ToSingle(data, 13);
                                    player.Velocity.Y = BitConverter.ToSingle(data, 17);
                                    player.Position.X = BitConverter.ToSingle(data, 21);
                                    player.Position.Y = BitConverter.ToSingle(data, 25);
                                    TimeSpan total = new TimeSpan(BitConverter.ToInt64(data, 29));
                                    TimeSpan elapsed = new TimeSpan(BitConverter.ToInt64(data, 37));
                                    GameTime gt = new GameTime(total, elapsed);
                                    player.Update(gt);
                                    //Console.WriteLine("Player Info: Name:{0} Type:{1}", player.Name, player.Type); 
                                    if (host)
                                    {
                                        for (byte i = 0; i < Players.Length; i++)
                                            if (i != pID && i != PlayerID && Players[i] != null)
                                            {
                                                try
                                                {
                                                    listenClient.Send(data, data.Length, Players[i].clientIPE);
                                                }
                                                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                                            }
                                    }
                                }
                            }
                        }
                        else if (type == PacketType.Disconnect)
                        {
                            byte pID = data[1];
                            Console.WriteLine("player {0} disconnected: ({1})", pID, eRemote);
                            Players[pID] = null;
                            if (host)
                            {
                                for (byte i = 0; i < Players.Length; i++)
                                    if (i != pID && i != PlayerID && Players[i] != null)
                                    {
                                        try
                                        {
                                            listenClient.Send(data, data.Length, Players[i].clientIPE);
                                        }
                                        catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                                    }
                            }
                        }
                        else if (type == PacketType.Ping)
                        {
                            if (host)
                            {                           
                                DateTime now = DateTime.Now;
                                byte pID = data[1];
                                if (Players[pID] != null)                                
                                    Players[pID].Ping = (int)(now - lastPing).TotalMilliseconds;                                
                            }
                            else
                            {
                                byte[] pong = new byte[] { (byte)PacketType.Ping, PlayerID };
                                try
                                {
                                    listenClient.Send(pong, pong.Length);
                                }
                                catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                            }
                        }
                        else if (type == PacketType.Info)
                        {
                            List<byte> infos = new List<byte>();
                            infos.Add((byte)PacketType.Info);
                            byte count = 0;
                            foreach (Player p in Players)
                                if (p != null)
                                    count++;
                            infos.Add(count);
                            infos.AddRange(UTF8Encoding.UTF8.GetBytes(level.FileName));
                            infos.Add(0);
                            try
                            {
                                listenClient.Send(infos.ToArray(), infos.Count, eRemote);
                            }
                            catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                        }
                        else if (type == PacketType.PlayerKill)
                        {
                            byte pID = data[1];
                            if (Players[pID] != null)
                            {
                                Players[pID].Lives--;
                                Players[pID].DeathTime = 1500;
                                for (byte p1 = 0; p1 < Players.Length; p1++)
                                {
                                    var ply1 = Players[p1];
                                    if (ply1 != null && ply1.Lives != 0)
                                    {
                                        byte alive = 0;
                                        byte playing = 0;
                                        for (byte p2 = 0; p2 < Players.Length; p2++)
                                        {
                                            var ply2 = Players[p2];
                                            if (ply2 != null && p2 != p1)
                                            {
                                                playing++;
                                                if (ply2.Lives != 0)
                                                {
                                                    alive++;                                                    
                                                }
                                            }
                                        }
                                        if (playing != 0 && alive == 0)
                                        {
                                            Controls.Add(new MessageBox("winbox", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 150, GraphicsDevice.Viewport.Height / 2 - 75, 300, 150), Main.borders, Main.closeButton, String.Format("{0} wins!", ply1.Name), CloseWinBox));
                                            ShowCursor = true;
                                        }
                                    }
                                }
                            }
                        }
                        else if (type == PacketType.LevelChange)
                        {
                            int len;
                            string lvlname = Main.GetString(ref data, 1, out len);
                            LoadLevel(lvlname);
                            SendPlayerToRandomEmpty(Players[PlayerID]);
                            for (byte p = 0; p < Players.Length; p++)
                                if (Players[p] != null)
                                {
                                    Players[p].Lives = 5;
                                    Players[p].DeathTime = 3000;
                                }
                            try
                            {
                                var control = Controls.Find(c => c.Name == "winbox");
                                if (control != null)
                                    Controls.Remove(control);
                            }
                            catch { }
                        }

                    }
                }
        }
        public void LoadLevel(string levelName)
        {
            lock (_levelLock)
            {
                level = new Level();
                level.ReadFile(Game.GraphicsDevice, levelName);
                Main.graphics.PreferredBackBufferWidth = TileW * level.width;
                Main.graphics.PreferredBackBufferHeight = TileH * level.height + StatusBarH;
            }
            try
            {
                var statusBar = Controls.First(c => c.Name == "statusbar");
                statusBar.Position.Width = TileW * level.width;
            }
            catch { }
            Main.graphicsChanged = true;
        }
        public bool HostGame(string levelName)
        {
            host = true;
            Console.WriteLine("loading lvl");
            LoadLevel(levelName);
            PlayerID = 0;
            Players[PlayerID] = new Player(PlayerID, "Player 1", this);
            Players[PlayerID].Type = (byte)random.Next(0, 20);
            Players[PlayerID].color = new Color(random.Next(1, 255), random.Next(1, 255), random.Next(1, 255));
            Players[PlayerID].Direction = 1;
            int tile = RandomEmptyTile();
            Players[PlayerID].Position = new Vector2((tile % level.width) * TileW, (tile / level.width) * TileH);
            Console.WriteLine("done");
                     
            listenClient = new UdpClient(15000);
            listenThread = new Thread(Listener);
            listenThread.Start();

            Console.WriteLine("adding to server list");
            // -------------- add to server list --------------------------
            string connectionString = "SERVER=84.255.249.171;PORT=3306;DATABASE=jumpman;UID=jumpman;PASSWORD=jumpmanpass;";
            MySqlConnection db = new MySqlConnection(connectionString);
            try
            {
                db.Open();
                MySqlCommand command = new MySqlCommand("INSERT INTO Servers (IP) SELECT SUBSTRING_INDEX(host,':',1) AS 'IP' FROM information_schema.processlist WHERE ID=connection_id()", db);
                command.ExecuteNonQuery();
                db.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }

            Console.WriteLine("done");
            // -------------------------------------------------
            return true;
        }
        protected override void LoadContent()
        {

            PlayerTexture = Game.Content.Load<Texture2D>("charset1");
            background = Game.Content.Load<Texture2D>(@"Backgrounds\cloudsinthedesert");
            int w;
            if (level != null)
                w = TileW * level.width;
            else 
                w = GraphicsDevice.Viewport.Width;
            Controls.Add(new FrameBox("statusbar", new Rectangle(0, 0, w, StatusBarH), Main.borders, null));

            base.LoadContent();
        }
        public void Abort()
        {
            ServerBrowser sb = new ServerBrowser(Game);
            Game.Components.Add(sb);
            sb.Controls.Add(new MessageBox("msgBox", new Rectangle(sb.frameRect.Left + 100, sb.frameRect.Center.Y - 100, sb.frameRect.Width - 200, 200), Main.borders, Main.closeButton, "Connection to Host lost.", sb.CloseBox));
            this.Dispose(true);
            Game.Components.Remove(this);
            listenThread.Abort();
        }
        public override void Update(GameTime gameTime)
        {
            if (level == null)
                return;
            KeyboardState keyState = Keyboard.GetState();
            if (keyState.IsKeyDown(Keys.Escape))
            {
                TitleScreen ts = new TitleScreen(Game);
                Game.Components.Add(ts);
                this.Dispose(true);
                Game.Components.Remove(this);
            }

            var player = Players[PlayerID];
            MoveFlags moveFlags = 0;

            if (keyState.IsKeyDown(Keys.Up) || keyState.IsKeyDown(Keys.W) || keyState.IsKeyDown(Keys.Space))
                moveFlags |= MoveFlags.Up;

            if (keyState.IsKeyDown(Keys.Down) || keyState.IsKeyDown(Keys.S))
                moveFlags |= MoveFlags.Down;

            if (keyState.IsKeyDown(Keys.Left) || keyState.IsKeyDown(Keys.A))
                moveFlags |= MoveFlags.Left;

            if (keyState.IsKeyDown(Keys.Right) || keyState.IsKeyDown(Keys.D))
                moveFlags |= MoveFlags.Right;

            if (moveFlags != player.moveFlags)
            {
                player.moveFlags = moveFlags;
                network.SendMovePkg(player, gameTime);
            }
            if (host && (DateTime.Now - lastPing).TotalMilliseconds >= 4000)
                network.SendPing();

            foreach (Player ply in Players)
                if (ply != null)
                    ply.Update(gameTime);

            if (host)
            {
                for (byte p1 = 0; p1 < Players.Length; p1++)
                {
                    var ply1 = Players[p1];
                    if (ply1 != null && ply1.Lives != 0 && ply1.Velocity.Y > 0)
                    {
                        byte alive = 0;
                        byte playing = 0;
                        for (byte p2 = 0; p2 < Players.Length; p2++)
                        {
                            var ply2 = Players[p2];
                            if (ply2 != null && p2 != p1)
                            {
                                playing++;
                                if (ply2.Lives != 0)
                                {
                                    alive++;
                                    if (ply2.DeathTime == 0)
                                    {
                                        Vector2 diff = ply2.Position - ply1.Position;
                                        if (Math.Abs(diff.X) < ply1.Width && diff.Y >= ply1.Height * 0.75f && diff.Y <= ply1.Height)
                                        {
                                            ply2.Lives--;
                                            ply2.DeathTime = 1500;
                                            network.SendKillPkg(p2);
                                            int tile = RandomEmptyTile();
                                            ply2.Velocity = Vector2.Zero;
                                            ply2.Acceleration = Vector2.Zero;
                                            ply2.Position = new Vector2((tile % level.width) * TileW, (tile / level.width) * TileH);
                                            network.SendMovePkg(ply2, gameTime);
                                        }
                                    }
                                }
                            }
                        }
                        if (playing != 0 && alive == 0)
                        {
                            Controls.Add(new MessageBox("winbox", new Rectangle(GraphicsDevice.Viewport.Width / 2 - 150, GraphicsDevice.Viewport.Height / 2 - 75, 300, 150), Main.borders, Main.closeButton, String.Format("{0} wins!", ply1.Name), CloseWinBox));
                            ShowCursor = true;
                        }
                    }
                }                   
            }
            lock (_levelLock)
            {
                if (level.animatedSets != null)
                {
                    foreach (AnimatedTileCollection tileset in level.animatedSets)
                        tileset.Update(gameTime);
                }
            }
            lock (Controls)
            {
                for (int i = Controls.Count - 1; i >= 0; i--)
                    Controls[i].Update();
            }
            base.Update(gameTime);
        }
        void CloseWinBox(MessageBox box)
        {
            lock (Controls)
            {
                Controls.Remove(box);
            }
            if (host)
            {
                string newlvl = Main.Levels[random.Next(0, Main.Levels.Length)];
                //string newlvl = Main.Levels[1];
                LoadLevel(newlvl);
                network.SendNewLvlPkg(newlvl);
                SendPlayerToRandomEmpty(Players[PlayerID]);
                for (byte p = 0; p < Players.Length; p++)
                    if (Players[p] != null)
                    {
                        Players[p].Lives = 5;
                        Players[p].DeathTime = 3000;
                    }
            }
        }
        public void SendPlayerToRandomEmpty(Player player)
        {
            int tile = RandomEmptyTile();
            player.Velocity = Vector2.Zero;
            player.Acceleration = Vector2.Zero;
            lock (_levelLock)
            {
                player.Position = new Vector2((tile % level.width) * TileW, (tile / level.width) * TileH);
            }
            network.SendMovePkg(player, new GameTime());
        }
        public int RandomEmptyTile()
        {
            bool invalidTile = true;
            int tile = 0;
            lock (_levelLock)
            {
                while (invalidTile)
                {
                    tile = random.Next(0, level.width * level.height);
                    invalidTile = false;
                    for (int i = 0; i < level.layers.Length; i++)
                    {
                        var layer = level.layers[i];
                        if (layer.type == LayerType.Floor || layer.type == LayerType.Collision)
                        {
                            if (layer.gIDs[tile] != -1)
                            {
                                invalidTile = true;
                                break;
                            }
                        }
                    }
                }
            }
            return tile;
        }

        public override void Draw(GameTime gameTime)
        {
            Main.spriteBatch.Begin(SpriteSortMode.Deferred, BlendState.AlphaBlend, SamplerState.PointClamp, DepthStencilState.Default, RasterizerState.CullCounterClockwise);
            // ------------- background ---------------------
            Main.spriteBatch.Draw(background, new Rectangle(0, 0, GraphicsDevice.Viewport.Width, GraphicsDevice.Viewport.Height), Color.White);
            // --------------- tiles ----------------------
            lock (_levelLock)
            {
                for (short y = 0; y < level.height; y++)
                {
                    for (short x = 0; x < level.width; x++)
                    {
                        short tile = (short)(x + y * level.width);
                        Rectangle dest = new Rectangle(x * TileW, y * TileH + StatusBarH, TileW, TileH);
                        foreach (Layer layer in level.layers)
                        {
                            short gID = layer.gIDs[tile];
                            if (gID != -1)
                            {
                                if (layer.type == LayerType.Animated)
                                {
                                    var tileSet = level.animatedSets[gID];
                                    byte frameIndex = (byte)((tileSet.FrameIndex + x) % tileSet.FrameCount);
                                    Main.spriteBatch.Draw(tileSet.texture, dest, new Rectangle(frameIndex * tileSet.tileW, 0, tileSet.tileW, tileSet.tileH), Color.White);
                                }
                                else
                                {
                                    var tileSet = level.getTileSetFromGID(gID);
                                    if (tileSet != null)
                                        Main.spriteBatch.Draw(tileSet.texture, dest, tileSet.GetSourceRect(gID), Color.White);
                                }
                            }
                        }
                    }
                }

                // --------- alive players ------------------
                for (int i = 0; i < Players.Length; i++)
                {
                    var player = Players[i];
                    if (player != null && player.Lives > 0)
                    {
                        Rectangle source = new Rectangle((player.Type % 6) * 48 + player.SpriteFrameIndex * 16, (player.Type / 6) * 72 + player.Direction * 18, 16, 18);
                        if (player.Position.X + player.Width > TileW * level.width)
                            Main.spriteBatch.Draw(PlayerTexture, new Rectangle((int)player.Position.X - TileW * level.width, (int)player.Position.Y + StatusBarH, player.Width, player.Height), source, Color.White);
                        if (player.Position.Y + player.Height > TileH * level.height)
                            Main.spriteBatch.Draw(PlayerTexture, new Rectangle((int)player.Position.X, (int)player.Position.Y - TileH * level.height + StatusBarH, player.Width, player.Height), source, Color.White);
                        Color pColor = Color.White;
                        if ((player.DeathTime / 100) % 2 != 0)
                            pColor *= 0.50f;
                        Main.spriteBatch.Draw(PlayerTexture, new Rectangle((int)player.Position.X, (int)player.Position.Y + StatusBarH, player.Width, player.Height), source, pColor);

                    }
                }
            }
            // ------------ controls ------------
            lock (Controls)
            {
                foreach (Control control in Controls)
                    control.Draw();
            }
            //-------------------------- Status bar ---------------
            int w4 = (GraphicsDevice.Viewport.Width - 16) / 4;
            for (int i = 0; i < Players.Length; i++)
            {
                var player = Players[i];
                if (player != null)
                {
                    int offset = 16 + i * w4;
                    string livesText = String.Format("{0}x", player.Lives);
                    Vector2 textSize = Main.MainFont.MeasureString(livesText);
                    Main.spriteBatch.DrawString(Main.MainFont, livesText, new Vector2(offset, 8), Color.Black, 0f, Vector2.Zero, 0.9f, SpriteEffects.None, 0);
                    offset += (int)textSize.X;
                    Rectangle portraint = new Rectangle((player.Type % 6) * 48 + 16, (player.Type / 6) * 72 + 2 * 18, 16, 18);
                    Rectangle portDest = new Rectangle(offset, 8, player.Width, StatusBarH - 16);
                    Main.spriteBatch.Draw(PlayerTexture, portDest, portraint, Color.White);
                    offset += player.Width + 8;
                    Main.spriteBatch.DrawString(Main.MainFont, player.Name, new Vector2(offset, 10), player.color, 0f, Vector2.Zero, 0.7f, SpriteEffects.None, 0);
                }
            }
            //---------cursor-----
            if (ShowCursor)
                Main.spriteBatch.Draw(Main.cursor, new Vector2(Main.mouseState.X, Main.mouseState.Y), Color.White);
            Main.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
