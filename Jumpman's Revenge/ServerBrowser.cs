﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using MySql.Data;
using MySql.Data.MySqlClient;
using System.Net;
using System.Net.Sockets;
using System.Threading;
using System.IO;

namespace Jumpman_s_Revenge
{
    class ServerEntry
    {
        public IPEndPoint ep;
        public int ping;
        public string lvlName;
        public byte playerCount;
    }
    class ServerBrowser : DrawableGameComponent
    {
        public List<Control> Controls = new List<Control>();
        public List<ServerEntry> Servers = new List<ServerEntry>();
        String[] IPs;
        int Selected = -1;
        public Rectangle frameRect;
        Texture2D highlight;
        Thread connThread;
        Thread refreshThread;
        UdpClient client;

        public ServerBrowser(Game game)
            : base(game)
        {
        }
        public override void Initialize()
        {
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (connThread != null && connThread.IsAlive)
                {
                    try
                    {
                        client.Close(); 
                        connThread.Abort();
                    }
                    catch { }
                }
                if (refreshThread != null && refreshThread.IsAlive)
                {
                    try
                    {
                        refreshThread.Abort();
                    }
                    catch { }
                }
            }
            base.Dispose(disposing);
        }
        protected override void LoadContent()
        {

            frameRect = new Rectangle(100, 100, GraphicsDevice.Viewport.Width - 200, GraphicsDevice.Viewport.Height - 300);

            Controls.Add(new FrameBox("frame", frameRect, Main.borders, null));

            Controls.Add(new Button("back", new Rectangle(frameRect.Right - 150, frameRect.Bottom + 50, 150, 50), Game.Content.Load<Texture2D>("button_green"), "Back", OnButtonClick));
            Controls.Add(new Button("refresh", new Rectangle(frameRect.Left, frameRect.Bottom + 50, 150, 50), Game.Content.Load<Texture2D>("button_green"), "Refresh", OnButtonClick));
            Controls.Add(new Button("join", new Rectangle(frameRect.Left + 200, frameRect.Bottom + 50, 150, 50), Game.Content.Load<Texture2D>("button_green"), "Join", OnButtonClick));

            highlight = new Texture2D(GraphicsDevice, 1, 1);
            highlight.SetData(new Color[] { Color.Gray });

            base.LoadContent();
        }
        public override void Update(GameTime gameTime)
        {
            lock (Controls)
            {
                for (int i = Controls.Count - 1; i >= 0; i--)
                    Controls[i].Update();
            }

            Point mousePos = new Point(Main.mouseState.X, Main.mouseState.Y);
            if (IPs != null && Main.mouseState.LeftButton == ButtonState.Pressed && mousePos.X > frameRect.Left && mousePos.X < frameRect.Right && mousePos.Y > frameRect.Top && mousePos.Y < frameRect.Bottom)
            {
                int selected = (mousePos.Y - frameRect.Y) / 50;
                if (selected < IPs.Length)
                    Selected = selected;                
            }
            base.Update(gameTime);
        }
        void OnButtonClick(Button button)
        {
            lock (Controls)
            {
                if (button.Name == "back")
                {
                    Game.Components.Remove(this);
                    Game.Components.Add(new TitleScreen(Game));
                    this.Dispose();
                }
                else if (button.Name == "refresh")
                {
                    if (refreshThread == null || !refreshThread.IsAlive)
                    {
                        Controls.Add(new MessageBox("refreshBox", new Rectangle(frameRect.Left + 100, frameRect.Center.Y - 100, frameRect.Width - 200, 200), Main.borders, Main.closeButton, "refreshing...", CloseBox));
                        refreshThread = new Thread(RefreshList);
                        refreshThread.Start();
                    }
                }
                else if (button.Name == "join")
                {
                    if (Selected != -1)
                    {
                        if (connThread != null && connThread.IsAlive)
                        {
                            try
                            {
                                connThread.Abort();
                                client.Close();
                            }
                            catch { }
                        }
                        Controls.Add(new MessageBox("conmsgbox", new Rectangle(frameRect.Left + 100, frameRect.Center.Y - 100, frameRect.Width - 200, 200), Main.borders, Main.closeButton, "connecting...", CloseBox));
                        connThread = new Thread(TryConnect);
                        connThread.Start();
                    }
                }
            }
        }
        public void RefreshList()
        {
            string connectionString = "SERVER=84.255.249.171;PORT=3306;DATABASE=jumpman;UID=jumpman;PASSWORD=jumpmanpass;";
            MySqlConnection db = new MySqlConnection(connectionString);
            try
            {
                db.Open();
                MySqlCommand command = new MySqlCommand("SELECT * FROM Servers", db);
                var reader = command.ExecuteReader();
                List<string> ips = new List<string>();
                while (reader.Read())
                {
                    ips.Add(reader.GetString(1));
                }
                IPs = ips.ToArray();
                reader.Close();
                db.Close();
                lock (Servers)
                {
                    Servers = new List<ServerEntry>();
                }
                for (int i = 0; i < IPs.Length; i++)
                {
                    try
                    {
                        client = new UdpClient();
                        IPAddress ip;
                        if (!IPAddress.TryParse(IPs[i], out ip))
                            ip = Dns.GetHostAddresses(IPs[i])[0];
                        IPEndPoint eRemote = new IPEndPoint(ip, 15000);
                        client.Connect(eRemote);
                        client.Client.ReceiveTimeout = 3000;
                        byte[] datagram = new byte[1];
                        datagram[0] = (byte)PacketType.Info;
                        DateTime before = DateTime.Now;
                        client.Send(datagram, datagram.Length);
                        byte[] data = client.Receive(ref eRemote);
                        int p = (int)((DateTime.Now - before).TotalMilliseconds);
                        if (data[0] == (byte)PacketType.Info)
                        {
                            byte pCount = data[1];
                            int len;
                            string name = Main.GetString(ref data, 2, out len);
                            ServerEntry server = new ServerEntry() { ep = eRemote, lvlName = name, ping = p, playerCount = pCount };
                            lock (Servers)
                            {
                                Servers.Add(server);
                            }
                        }
                    }
                    catch { }
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.ToString());
            }
            finally
            {
                lock (Controls)
                {
                    for (int i = Controls.Count - 1; i >= 0; i--)
                    {
                        MessageBox box = Controls[i] as MessageBox;
                        if (box != null && box.Name == "refreshBox")
                            Controls.RemoveAt(i);
                    }
                }
                refreshThread = null;
            }
        }

        public void TryConnect()
        {
            String msg = "";
            try
            {
                ServerEntry server = Servers[Selected];
                client = new UdpClient();
                IPEndPoint eRemote = server.ep;
                client.Connect(eRemote);
                byte[] datagram = new byte[1];
                datagram[0] = (byte)PacketType.Connecting;
                client.Send(datagram, datagram.Length);
                byte[] data = client.Receive(ref eRemote);
                int index = 0;
                if (data.Length > 0 && data[index++] == (byte)PacketType.Connecting)
                {
                    byte pID = data[index++];
                    Game.Components.Remove(this);
                    GameComponent game = new GameComponent(Game);
                    int len;
                    string lvlname = Main.GetString(ref data, index, out len);
                    game.LoadLevel(lvlname);

                    #region ReadLevelStream
                    //------------- read level info -------------
                    /*
                    game.level = new Level();
                    int strLen;
                    game.level.Name = Main.GetString(ref data, index, out strLen);
                    index += strLen;
                    game.level.width = BitConverter.ToInt16(data, index);
                    index += 2;
                    game.level.height = BitConverter.ToInt16(data, index);
                    index += 2;
                    game.level.tileSets = new TileCollection[data[index++]];
                    for (int i = 0; i < game.level.tileSets.Length; i++)
                    {
                        String setName = Main.GetString(ref data, index, out strLen);
                        index += strLen;
                        game.level.tileSets[i] = new TileCollection(Game.Content, setName, data[index++], data[index++], BitConverter.ToInt16(data, index));
                        index += 2;
                    }
                    game.level.animatedSets = new AnimatedTileCollection[data[index++]];
                    for (int i = 0; i < game.level.animatedSets.Length; i++)
                    {
                        String setName = Main.GetString(ref data, index, out strLen);
                        index += strLen;
                        game.level.animatedSets[i] = new AnimatedTileCollection(Game.Content, setName, data[index++], data[index++], BitConverter.ToSingle(data, index));
                        game.level.tileSets[i] = new TileCollection(Game.Content, setName, data[index++], data[index++], BitConverter.ToInt16(data, index));
                        index += 4;
                    }
                    game.level.layers = new Layer[data[index++]];
                    for (int i = 0; i < game.level.layers.Length; i++)
                    {
                        game.level.layers[i] = new Layer();
                        game.level.layers[i].type = (LayerType)data[index++];
                        game.level.layers[i].gIDs = new short[game.level.width * game.level.height];
                        for (int j = 0; j < game.level.layers[i].gIDs.Length; j++)
                        {
                            game.level.layers[i].gIDs[j] = BitConverter.ToInt16(data, index);
                            index += 2;
                        }
                    }
                    */
                    #endregion

                    // ------------- send player info --------------
                    game.PlayerID = pID;
                    Random r = new Random();
                    game.Players[pID] = new Player(pID, String.Format("Player {0}", pID + 1), game);
                    game.Players[pID].Type = (byte)r.Next(0, 20);
                    game.Players[pID].color = new Color(r.Next(1, 255), r.Next(1, 255), r.Next(1, 255));

                    List<byte> pInfoData = new List<byte>();
                    pInfoData.Add((byte)PacketType.PlayerInfo);
                    pInfoData.Add(pID);
                    var strData = UTF8Encoding.UTF8.GetBytes(game.Players[game.PlayerID].Name);
                    pInfoData.AddRange(strData);
                    pInfoData.Add(0);
                    pInfoData.Add(game.Players[game.PlayerID].Type);
                    pInfoData.Add(game.Players[pID].color.R);
                    pInfoData.Add(game.Players[pID].color.G);
                    pInfoData.Add(game.Players[pID].color.B);
                    pInfoData.Add(game.Players[pID].Lives);
                    datagram = pInfoData.ToArray();
                    client.Send(datagram, datagram.Length); 

                    // ---------------- add listener --------------
                    game.listenClient = client;
                    game.listenThread = new Thread(game.Listener);
                    game.listenThread.Start();
                    Game.Components.Add(game);

                    connThread = null;
                    this.Dispose(true);
                    return;
                }
                else
                {
                    client.Close();
                    msg = "Server full";
                }
            }
            catch (SocketException ex)
            {
                Console.WriteLine(ex.ToString());
                msg = "Could not connect";
            }
            lock (Controls)
            {
                for (int i = Controls.Count - 1; i >= 0; i--)
                {
                    MessageBox mb = Controls[i] as MessageBox;
                    if (mb != null && mb.Name == "conmsgbox")
                        mb.Text = msg;
                }
            }      
        }
        public void CloseBox(MessageBox box)
        {
            if (box.Name == "conmsgbox")
            {
                if (connThread != null && connThread.IsAlive)
                {
                    try
                    {
                        client.Close();
                        connThread.Abort();
                    }
                    catch { }
                    connThread = null;
                }
            }
            else if (box.Name == "refreshbox")
            {                
                try
                {
                    refreshThread.Abort();
                }
                catch { }
                refreshThread = null;                
            }
            lock (Controls)
            {
                Controls.Remove(box);
            }
        }
        public void RefreshServers()
        {
            
        }

        public override void Draw(GameTime gameTime)
        {
            Main.spriteBatch.Begin();
            lock (Controls)
            {
                foreach (Control control in Controls)
                    control.Draw();
            }

            lock (Servers)
            {
                for (int i = 0; i < Servers.Count; i++)
                {
                    ServerEntry server = Servers[i];
                    float size = 0.7f;
                    if (Selected == i)
                    {
                        Main.spriteBatch.Draw(highlight, new Rectangle(frameRect.X + 8, frameRect.Y + 8 + i * 50, frameRect.Width - 16, 50), Color.White);
                    }
                    Vector2 textSize = Main.MainFont.MeasureString(server.ep.ToString());
                    int posY = frameRect.Y + i * 50 + 16;
                    int posX = frameRect.X + 16;
                    Main.spriteBatch.DrawString(Main.MainFont, server.ep.ToString(), new Vector2(posX, posY), Color.Black, 0f, Vector2.Zero, size, SpriteEffects.None, 0);
                    posX += (int)textSize.X;
                    var split = server.lvlName.Split('\\');
                    string lvl = split[split.Length - 1];
                    textSize = Main.MainFont.MeasureString(lvl);
                    Main.spriteBatch.DrawString(Main.MainFont, lvl, new Vector2(posX, posY), Color.Black, 0f, Vector2.Zero, size, SpriteEffects.None, 0);
                    posX += (int)textSize.X;
                    string players = String.Format("Playes: {0}/4", server.playerCount);
                    textSize = Main.MainFont.MeasureString(players);
                    Main.spriteBatch.DrawString(Main.MainFont, players, new Vector2(posX, posY), Color.Black, 0f, Vector2.Zero, size, SpriteEffects.None, 0);
                    posX += (int)textSize.X;
                    Main.spriteBatch.DrawString(Main.MainFont, String.Format("Ping: {0}",server.ping), new Vector2(posX, posY), Color.Black, 0f, Vector2.Zero, size, SpriteEffects.None, 0);
                }
            }
            
            Main.spriteBatch.Draw(Main.cursor, new Vector2(Main.mouseState.X, Main.mouseState.Y), Color.White);
            Main.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
