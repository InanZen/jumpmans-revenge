﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


namespace Jumpman_s_Revenge
{
    enum PacketType
    {
        Info = 0,
        Connecting = 1,
        PlayerInfo = 2,
        PlayerMove = 3,
        Disconnect = 4,
        Ping = 5,
        PlayerKill = 6,
        LevelChange = 7
    }
    class Network
    {
        public GameComponent Game;
        public Network(GameComponent game)
        {
            this.Game = game;
        }
        public void SendMovePkg(Player player, Microsoft.Xna.Framework.GameTime gameTime)
        {
            List<byte> movePkg = new List<byte>();
            movePkg.Add((byte)PacketType.PlayerMove);
            movePkg.Add(player.Index);
            movePkg.AddRange(BitConverter.GetBytes(++player.moveIndex));
            movePkg.Add((byte)player.moveFlags);
            movePkg.AddRange(BitConverter.GetBytes(player.Acceleration.X));
            movePkg.AddRange(BitConverter.GetBytes(player.Acceleration.Y));
            movePkg.AddRange(BitConverter.GetBytes(player.Velocity.X));
            movePkg.AddRange(BitConverter.GetBytes(player.Velocity.Y));
            movePkg.AddRange(BitConverter.GetBytes(player.Position.X));
            movePkg.AddRange(BitConverter.GetBytes(player.Position.Y));
            movePkg.AddRange(BitConverter.GetBytes(gameTime.TotalGameTime.Ticks));
            movePkg.AddRange(BitConverter.GetBytes(gameTime.ElapsedGameTime.Ticks));

            byte[] data = movePkg.ToArray();
            if (Game.host)
            {
                for (byte i = 0; i < Game.Players.Length; i++)
                    if (i != Game.PlayerID && Game.Players[i] != null)
                    {
                        try
                        {
                            Game.listenClient.Send(data, data.Length, Game.Players[i].clientIPE);
                        }
                        catch (Exception ex)
                        {
                            Console.WriteLine("client disconnected");
                            Console.WriteLine(ex.ToString());
                            Game.Players[i] = null;
                        }
                    }
            }
            else
                try
                {
                    Game.listenClient.Send(data, data.Length);
                }
                catch
                {
                    Game.Abort();
                }
        }
        public void SendPing()
        {
            Game.lastPing = DateTime.Now;
            byte[] data = new byte[1];
            data[0] = (byte)PacketType.Ping;
            for (byte i = 0; i < Game.Players.Length; i++)
                if (i != Game.PlayerID && Game.Players[i] != null)
                {
                    try
                    {
                        Game.listenClient.Send(data, data.Length, Game.Players[i].clientIPE);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("client disconnected");
                        Console.WriteLine(ex.ToString());
                        Game.Players[i] = null;
                    }
                }
        }
        public void SendKillPkg(byte pID)
        {
            byte[] data = new byte[2];
            data[0] = (byte)PacketType.PlayerKill;
            data[1] = pID;
            for (byte i = 0; i < Game.Players.Length; i++)
                if (i != Game.PlayerID && Game.Players[i] != null)
                {
                    try
                    {
                        Game.listenClient.Send(data, data.Length, Game.Players[i].clientIPE);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("client disconnected");
                        Console.WriteLine(ex.ToString());
                        Game.Players[i] = null;
                    }
                }
        }
        public void SendNewLvlPkg(string level)
        {
            List<byte> dataList = new List<byte>();
            dataList.Add((byte)PacketType.LevelChange);
            dataList.AddRange(UTF8Encoding.UTF8.GetBytes(level));
            dataList.Add(0);
            byte[] data = dataList.ToArray();
            for (byte i = 0; i < Game.Players.Length; i++)
                if (i != Game.PlayerID && Game.Players[i] != null)
                {
                    try
                    {
                        Game.listenClient.Send(data, data.Length, Game.Players[i].clientIPE);
                    }
                    catch (Exception ex)
                    {
                        Console.WriteLine("client disconnected");
                        Console.WriteLine(ex.ToString());
                        Game.Players[i] = null;
                    }
                }
        }
    }
}
