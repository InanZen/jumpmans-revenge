﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using System.Net;

namespace Jumpman_s_Revenge
{
    [Flags]
    enum MoveFlags
    {
        Up = 1,
        Down = 2,
        Left = 4,
        Right = 8
    }
    class Player
    {
        public String Name;
        public byte Index;
        public Color color;
        public byte Type;
        public byte Direction;
        public int Width { get { return (int)(game.TileW * 0.9f); } }
        public int Height { get { return (int)(game.TileW * 1.2f); } }
        public Vector2 Position = new Vector2();
        public Vector2 Acceleration = new Vector2();
        public Vector2 Velocity = new Vector2();
        public Vector2 MaxVelocity = new Vector2(0.5f, 3f);
        public MoveFlags moveFlags;
        public bool OnGround;
        public int JumpTime;
        public int MaxJumpTime = 200;
        public float JumpPower = 0.006f;

        public byte SpriteFrameIndex { get { return frameIDs[curFrame]; } }
        public byte curFrame;
        public byte[] frameIDs;
        public float TotalTime = 0;
        public float FrameTime = 50f;

        public GameComponent game;

        public Texture2D colTexture;
        public IPEndPoint clientIPE;
        public UInt16 moveIndex;
        public int Ping;
        public byte Lives = 5;
        public short DeathTime = 0;

        public Player(byte index, string name, GameComponent gameInstance)
        {
            this.game = gameInstance;
            this.Name = name;
            this.Index = index;
            this.frameIDs = new byte[] { 0, 1, 2, 1 };
            this.curFrame = 1;
        }

        public void Update(GameTime gameTime)
        {
            try
            {
                float t = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                TotalTime += t;
                if (DeathTime > 0)
                {
                    DeathTime -= (short)t;
                    if (DeathTime < 0)
                        DeathTime = 0;
                }

                Level level = game.level;
                if (level == null)
                    return;

                if (Velocity.X != 0 && TotalTime >= 25 / Math.Abs(Velocity.X))
                {
                    curFrame++;
                    curFrame = (byte)(curFrame % frameIDs.Length);
                    TotalTime = 0;
                    // TotalTime -= FrameTime;
                }

                Acceleration.Y = 0.0025f;   // gravity   
                if (moveFlags.HasFlag(MoveFlags.Up)) // jummp
                {
                    if (OnGround)
                    {
                        Acceleration.Y += -1 * JumpPower;
                        JumpTime = 0;
                    }
                    else if (JumpTime < MaxJumpTime)
                    {
                        float timeMod = 1f - ((float)JumpTime / MaxJumpTime) / 3;
                        Acceleration.Y += -1 * JumpPower * timeMod;
                        JumpTime += gameTime.ElapsedGameTime.Milliseconds;
                    }
                }
                else
                    JumpTime = MaxJumpTime;

                if (moveFlags.HasFlag(MoveFlags.Left))
                {
                    float groundmod = 1f;
                    float reversemod = 1f;
                    if (!OnGround)
                        groundmod = 0.75f;
                    if (Velocity.X > 0)
                        reversemod = 2f;                        
                    Acceleration.X = -0.0006f * groundmod * reversemod;
                }
                else if (moveFlags.HasFlag(MoveFlags.Right))
                {
                    float groundmod = 1f;
                    float reversemod = 1f;
                    if (!OnGround)
                        groundmod = 0.8f;
                    if (Velocity.X < 0)
                        reversemod = 3f;
                    Acceleration.X = 0.0006f * groundmod * reversemod;
                }
                else
                {
                    float groundmod = 1f;
                    if (!OnGround)
                        groundmod = 0.4f;
                    Acceleration.X += -0.00075f * Math.Sign(Velocity.X) * groundmod;
                    float deltaV = Acceleration.X * t;
                    if (Math.Sign(Velocity.X) != Math.Sign(Velocity.X + deltaV))
                    {
                        Acceleration.X = 0;
                        Velocity.X = 0;
                    }
                }

                Velocity.Y += Acceleration.Y * t;
                Velocity.Y = MathHelper.Clamp(Velocity.Y, MaxVelocity.Y * -1, MaxVelocity.Y);

                float newPosY = Position.Y + Velocity.Y * t;
                if (newPosY < 0)
                    newPosY += game.level.height * game.TileH;
                else if (newPosY > game.level.height * game.TileH)
                    newPosY = 0;

                int tileX = (int)(Position.X / game.TileW);
                int tileY = (int)(newPosY / game.TileH);
                Vector2 offsetInPixels = new Vector2(Position.X % game.TileW, newPosY % game.TileH);
                Vector2 offsetInProportion = new Vector2(offsetInPixels.X / game.TileW, offsetInPixels.Y / game.TileH);
                Vector2 widthInProportion = new Vector2((float)Width / game.TileW, (float)Height / game.TileH);
                int tile = tileX + tileY * level.width;
                if (Collides(level, tile, offsetInProportion, widthInProportion, 255))
                {
                    if (Velocity.Y >= 0)
                    {
                        // Console.WriteLine("collison BOTTOM");
                        OnGround = true;
                    }
                    else
                    {
                        //Console.WriteLine("collison TOP");
                    }
                    Velocity.Y = 0;
                }
                else
                {
                    Position.Y = newPosY;
                    OnGround = false;
                }

                Velocity.X += Acceleration.X * t;
                Velocity.X = MathHelper.Clamp(Velocity.X, MaxVelocity.X * -1, MaxVelocity.X);
                if (Velocity.X == 0)
                {
                    Direction = 2;
                    curFrame = 1;
                }
                else
                {
                    if (Velocity.X > 0)
                        Direction = 1;
                    else
                        Direction = 3;

                    float newPosX = Position.X + Velocity.X * t;
                    if (newPosX < 0)
                        newPosX += game.level.width * game.TileW;
                    else if (newPosX > game.level.width * game.TileW)
                        newPosX = 0;

                    tileX = (int)(newPosX / game.TileW);
                    tileY = (int)(Position.Y / game.TileH);
                    offsetInPixels = new Vector2(newPosX % game.TileW, Position.Y % game.TileH);
                    offsetInProportion = new Vector2(offsetInPixels.X / game.TileW, offsetInPixels.Y / game.TileH);
                    widthInProportion = new Vector2((float)Width / game.TileW, (float)Height / game.TileH);
                    tile = tileX + tileY * level.width;
                    if (Collides(level, tile, offsetInProportion, widthInProportion, 255))
                    {
                        offsetInProportion.Y -= 0.2f;
                        if (offsetInProportion.Y < 0 || Collides(level, tile, offsetInProportion, widthInProportion, 255))
                            Velocity.X = 0;
                        else
                        {
                            Position.X = newPosX;
                            Position.Y -= 0.2f * game.TileH;
                        }
                    }
                    else
                        Position.X = newPosX;
                }
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }
        public bool Collides(Level level, int tile, Vector2 offsetInProportion, Vector2 widthInProportion, byte treshold)
        {
            for (int l = 0; l < level.layers.Length; l++)
            {
                Layer layer = level.layers[l];
                if (layer.type == LayerType.Floor || layer.type == LayerType.Collision)
                {
                    // count = 0;
                    if (tile >= layer.gIDs.Length || tile < 0)
                        return false;
                    if (offsetInProportion.X + widthInProportion.X >= 1f)// spill into right tile
                    {
                        int newtile = tile + 1;
                        if (newtile % level.width == 0)
                            newtile -= level.width;
                        if (Collides(level, newtile, new Vector2(0, offsetInProportion.Y), new Vector2(widthInProportion.X - 1f + offsetInProportion.X, widthInProportion.Y), treshold))
                            return true;
                        widthInProportion.X = 1f - offsetInProportion.X;
                    }
                    if (offsetInProportion.Y + widthInProportion.Y >= 1f)// spill into bottom tile
                    {
                        int newtile = tile + level.width;
                        if (newtile >= level.width * level.height)
                            newtile = newtile % level.width;
                        if (Collides(level, newtile, new Vector2(offsetInProportion.X, 0), new Vector2(widthInProportion.X, widthInProportion.Y - 1f + offsetInProportion.Y), treshold))
                            return true;
                        widthInProportion.Y = 1f - offsetInProportion.Y;
                    }

                    short gid = layer.gIDs[tile];
                    if (gid != -1)
                    {
                        TileCollection tileSet = level.getTileSetFromGID(gid);
                        Rectangle source = tileSet.GetSourceRect(gid);

                        Point offsetScaled = new Point((int)(offsetInProportion.X * tileSet.tileW), (int)(offsetInProportion.Y * tileSet.tileH));
                        int widthScaled = (int)(widthInProportion.X * tileSet.tileW);
                        int heightScaled = (int)(widthInProportion.Y * tileSet.tileH);
                        if (widthScaled == 0 || heightScaled == 0)
                            return false;

                        Rectangle newsource = new Rectangle(source.X + offsetScaled.X, source.Y + offsetScaled.Y, widthScaled, heightScaled);
                        colTexture = new Texture2D(game.GraphicsDevice, widthScaled, heightScaled);
                        Color[] colors = new Color[widthScaled * heightScaled];
                        tileSet.texture.GetData(0, newsource, colors, 0, colors.Length);
                        for (int i = 0; i < colors.Length; i++)
                            if (colors[i].A >= treshold)
                            {
                                return true;
                            }
                    }
                }
            }
            return false;
        }


        public void UpdateAnimation(GameTime gameTime)
        {
            float sinceLast = (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            TotalTime += sinceLast;
            if (TotalTime >= FrameTime)
            {
                curFrame++;
                curFrame = (byte)(curFrame % frameIDs.Length);
                TotalTime -= FrameTime;
            }
        }


    }
}
