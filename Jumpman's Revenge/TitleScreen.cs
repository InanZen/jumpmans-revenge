using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Jumpman_s_Revenge
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class TitleScreen : Microsoft.Xna.Framework.DrawableGameComponent
    {

        List<Control> Controls = new List<Control>();

        public TitleScreen(Game game)
            : base(game)
        {
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            base.Initialize();
        }
        protected override void LoadContent()
        {

            int frameW = 500;
            int frameH = 500;
            int w2 = GraphicsDevice.Viewport.Width / 2;
            int h2 = GraphicsDevice.Viewport.Height / 2;
            int frameX = w2 - frameW / 2;
            int frameY = h2 - frameH / 2;
            
            Controls.Add(new FrameBox("frame", new Rectangle(frameX, frameY, frameW, frameH), Main.borders, null));
            Controls.Add(new Button("newgame", new Rectangle(frameX + 50, frameY + 50, frameW - 100, 50), Game.Content.Load<Texture2D>("button_green"), "New Game", OnButtonClick));
            Controls.Add(new Button("joingame", new Rectangle(frameX + 50, frameY + 150, frameW - 100, 50), Game.Content.Load<Texture2D>("button_green"), "Join Game", OnButtonClick));
            Controls.Add(new Button("exit", new Rectangle(frameX + 50, frameY + frameW - 100, frameW - 100, 50), Game.Content.Load<Texture2D>("button_green"), "Exit", OnButtonClick));

            base.LoadContent();
        }
        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            for (int i = Controls.Count - 1; i >= 0; i--)
                Controls[i].Update();
            base.Update(gameTime);
        }
        void OnButtonClick(Button button)
        {
            if (button.Name == "exit")
            {
                Game.Exit();
            }
            else if (button.Name == "newgame")
            {
                Game.Components.Remove(this);
                GameComponent game = new GameComponent(Game);
                Game.Components.Add(game);
                game.HostGame(Main.Levels[2]);
            }
            else if (button.Name == "joingame")
            {
                Game.Components.Remove(this);
                Game.Components.Add(new ServerBrowser(Game));
            }
        }
        public override void Draw(GameTime gameTime)
        {
            Main.spriteBatch.Begin();
            foreach (Control control in Controls)
                control.Draw();

            Main.spriteBatch.Draw(Main.cursor, new Vector2(Main.mouseState.X, Main.mouseState.Y), Color.White);
            Main.spriteBatch.End();
            base.Draw(gameTime);
        }
    }
}
