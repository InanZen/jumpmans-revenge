﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;


namespace Jumpman_s_Revenge
{
    /// <summary>
    /// This is a game component that implements IUpdateable.
    /// </summary>
    public class AudioManager : Microsoft.Xna.Framework.GameComponent
    {
        public Song[] SongList = new Song[1];
        public bool Muted = false;
        private Random rand;
        public AudioManager(Game game)
            : base(game)
        {
            
        }

        /// <summary>
        /// Allows the game component to perform any initialization it needs to before starting
        /// to run.  This is where it can query for any required services and load content.
        /// </summary>
        public override void Initialize()
        {
            rand = new Random();
            base.Initialize();
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                foreach (Song s in SongList)
                {
                    s.Dispose();
                }
            }
            base.Dispose(disposing);
        }

        /// <summary>
        /// Allows the game component to update itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        public override void Update(GameTime gameTime)
        {
            if (Game.IsActive && SongList.Length != 0)
            {
                if (MediaPlayer.State == MediaState.Paused)
                    MediaPlayer.Resume();
                switch (Main.gameState)
                {
                    case GameState.TitleScreen:
                        {
                            if (MediaPlayer.State != MediaState.Playing || MediaPlayer.Queue.ActiveSong.Name != SongList[0].Name)
                            {
                                MediaPlayer.Play(SongList[0]);
                            }
                            break;
                        }
                    case GameState.Options:
                        {
                            break;
                        }
                    case GameState.Game:
                        {
                            if (MediaPlayer.State != MediaState.Playing)
                            {
                                MediaPlayer.Play(SongList[rand.Next(0, SongList.Length)]);
                            }
                            break;
                        }
                }
            }
            else if (MediaPlayer.State == MediaState.Playing)
            {
                MediaPlayer.Pause();
            }
            base.Update(gameTime);
        }
    }
}
