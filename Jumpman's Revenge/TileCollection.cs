﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace Jumpman_s_Revenge
{
    public class TileCollection
    {
        public Texture2D texture;
        public byte tileW;
        public byte tileH;
        public int wTileCount { get { return texture.Width / tileW; } }
        public int hTileCount { get { return texture.Height / tileH; } }
        public int elementCount { get { return wTileCount * hTileCount; } }
        public short firstGID;

        public TileCollection(GraphicsDevice device, string FileName, byte TileW, byte TileH, short FirstGID)
        {
            string path = Path.Combine("Content", "TileSets", FileName);
            if (File.Exists(path))
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    this.texture = Texture2D.FromStream(device, stream);
                    this.texture.Name = FileName;
                }
            }
            else
                throw new FileNotFoundException("TileSet not found", path);
            this.tileW = TileW;
            this.tileH = TileH;
            this.firstGID = FirstGID;
        }
        public TileCollection(Texture2D Texture, byte TileW, byte TileH, short FirstGID)
        {
            this.texture = Texture;
            this.texture.Name = Texture.Name;
            this.tileW = TileW;
            this.tileH = TileH;
            this.firstGID = FirstGID;
        }
        public Rectangle GetSourceRect(short gID)
        {
            short setIndx = (short)(gID - this.firstGID);
            return new Rectangle((setIndx % this.wTileCount) * this.tileW, (setIndx / this.hTileCount) * this.tileH, this.tileW, this.tileH);
        }
    }
    public class AnimatedTileCollection
    {
        public Texture2D texture;
        public byte tileW;
        public byte tileH;
        public byte FrameIndex;
        public float frameTime;
        private float TotalTime = 0;
        public int FrameCount { get { return texture.Width / tileW; } }

        public AnimatedTileCollection(GraphicsDevice device, string FileName, byte TileW, byte TileH, float FrameTime)
        {
            string path = Path.Combine("Content", "TileSets", FileName);
            if (File.Exists(path))
            {
                using (FileStream stream = new FileStream(path, FileMode.Open, FileAccess.Read, FileShare.Read))
                {
                    this.texture = Texture2D.FromStream(device, stream);
                    this.texture.Name = FileName;
                }
            }
            else
                throw new FileNotFoundException("TileSet not found", path);
            this.tileW = TileW;
            this.tileH = TileH;
            this.frameTime = FrameTime;
        }
        public AnimatedTileCollection(Texture2D Texture, byte TileW, byte TileH, float FrameTime)
        {
            this.texture = Texture;
            this.texture.Name = Texture.Name;
            this.tileW = TileW;
            this.tileH = TileH;
            this.frameTime = FrameTime;
        }

        public void Update(GameTime gameTime)
        {
            float elapsed = (float)gameTime.ElapsedGameTime.TotalSeconds;
            TotalTime += elapsed;
            if (TotalTime > frameTime)
            {
                FrameIndex++;
                FrameIndex = (byte)(FrameIndex % FrameCount);
                TotalTime = 0;
            }
        }

    }
}
