﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using System.IO;

namespace Jumpman_s_Revenge
{
    public class Level
    {
        public string FileName;
        public string Name;
        public short width;
        public short height;
        public Point playerSpawn;
        public Layer[] layers;
        public TileCollection[] tileSets;
        public AnimatedTileCollection[] animatedSets;
        public TileCollection getTileSetFromGID(int gID)
        {
            for (int i = tileSets.Length - 1; i >= 0; i--)
                if (gID >= tileSets[i].firstGID)
                    return tileSets[i];
            return null;
        }
        public void ReadFile(Microsoft.Xna.Framework.Graphics.GraphicsDevice device, string path)
        {
            FileName = path;
           // using (var stream = TitleContainer.OpenStream(String.Format(@"Content\Levels\{0}.lvl", name)))
            using (var stream = TitleContainer.OpenStream(path))
            {
                using (BinaryReader reader = new BinaryReader(stream, Encoding.UTF8))
                {
                    byte MapVersion = reader.ReadByte();
                    this.Name = reader.ReadString();
                    this.width = reader.ReadInt16();
                    this.height = reader.ReadInt16();

                    int tileSetsCount;
                    if (MapVersion == 1)
                        tileSetsCount = reader.ReadInt32();
                    else
                        tileSetsCount = reader.ReadByte();
                    List<TileCollection> tileSets = new List<TileCollection>();
                    for (int i = 0; i < tileSetsCount; i++)
                    {
                        try
                        {
                            var tileSet = new TileCollection(device, reader.ReadString(), reader.ReadByte(), reader.ReadByte(), reader.ReadInt16());
                            tileSets.Add(tileSet);
                        }
                        catch (Exception ex) { Console.WriteLine(ex.ToString()); }
                    }
                    this.tileSets = tileSets.ToArray();

                    if (MapVersion == 1)
                        tileSetsCount = reader.ReadInt32();
                    else
                        tileSetsCount = reader.ReadByte();

                    List<AnimatedTileCollection> atileSets = new List<AnimatedTileCollection>();
                    for (int i = 0; i < tileSetsCount; i++)
                    {
                        try
                        {
                            var tileSet = new AnimatedTileCollection(device, reader.ReadString(), reader.ReadByte(), reader.ReadByte(), reader.ReadSingle());
                            atileSets.Add(tileSet);
                        }
                        catch { }
                    }
                    this.animatedSets = atileSets.ToArray();

                    if (MapVersion == 1)
                        this.layers = new Layer[reader.ReadInt32()];
                    else
                        this.layers = new Layer[reader.ReadByte()];   
                    for (int i = 0; i < this.layers.Length; i++)
                    {
                        this.layers[i] = new Layer();
                        this.layers[i].type = (LayerType)reader.ReadByte();
                        this.layers[i].gIDs = new short[this.width * this.height];
                        for (int j = 0; j < this.layers[i].gIDs.Length; j++)
                            this.layers[i].gIDs[j] = reader.ReadInt16();
                    }
                }
            }
        }
    }
    public class Layer
    {
        public LayerType type;
        public short[] gIDs;
    }
    public enum LayerType
    {
        Animated = 0,
        Floor = 1,
        Scenery = 2,
        Collision = 3,
        Floating = 4
    }
}
