using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace Jumpman_s_Revenge
{
    enum GameState
    {
        TitleScreen = 0,
        Options = 1,
        Game = 2,
        Pause = 3
    }

    /// <summary>
    /// This is the main type for your game
    /// </summary>
    public class Main : Microsoft.Xna.Framework.Game
    {
        internal static GraphicsDeviceManager graphics;
        internal static SpriteBatch spriteBatch;
        internal static SpriteFont MainFont;
        internal static GameState gameState;
        internal static bool graphicsChanged;
        internal static TileCollection borders;
        internal static String[] Levels;
        internal static Texture2D closeButton;
        internal static Texture2D cursor;
        internal static MouseState mouseState;

        public Main()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = 1024;
            graphics.PreferredBackBufferHeight = 800;
            Content.RootDirectory = "Content";
            IsFixedTimeStep = true;
            TargetElapsedTime = TimeSpan.FromMilliseconds(16);
            //System.Windows.Forms.Form f = (System.Windows.Forms.Form)System.Windows.Forms.Form.FromHandle(this.Window.Handle);
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            MainFont = Content.Load<SpriteFont>("MainFont");

            Texture2D borderTex = Content.Load<Texture2D>("borders");
            borderTex.Name = "borders";
            borders = new TileCollection(borderTex, 18, 18, 0);
            closeButton = Content.Load<Texture2D>("message_close");
            cursor = Content.Load<Texture2D>("cursor");
            string path = Path.Combine(Content.RootDirectory,"Levels");
            if (Directory.Exists(path))
            {
                /*List<string> files = new List<string>();
                foreach (string file in Directory.GetFiles(path))
                {
                    files.Add(Path.GetFileNameWithoutExtension(file));
                }
                Levels = files.ToArray();*/
                Levels = Directory.GetFiles(path);
            }

            gameState = GameState.TitleScreen;

            TitleScreen titleScr = new TitleScreen(this);
            Components.Add(titleScr);
            titleScr.Initialize();

            AudioManager audioMan = new AudioManager(this);
            List<Song> songList = new List<Song>();
            songList.Add(Content.Load<Song>("Choro_bavario"));
            audioMan.SongList = songList.ToArray();           
            Components.Add(audioMan);
            audioMan.Initialize();

        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// all content.
        /// </summary>
        protected override void UnloadContent()
        {
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            // Allows the game to exit
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed)
                this.Exit();
            if (graphicsChanged)
            {
                graphics.ApplyChanges();
                graphicsChanged = false;
            }

            mouseState = Mouse.GetState();
            
            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.Black);
            base.Draw(gameTime);
        }
        internal static void DrawString(string text, Vector2 pos, float scale)
        {
            if (text == null || text == "")
                return;
            Vector2 measure = Main.MainFont.MeasureString(text);
            Main.spriteBatch.DrawString(Main.MainFont, text, pos, Color.Black, 0f, measure * 0.5f, new Vector2(scale * 1.02f, scale * 1.15f), SpriteEffects.None, 0f);
            Main.spriteBatch.DrawString(Main.MainFont, text, pos, Color.AntiqueWhite, 0f, measure * 0.5f, scale, SpriteEffects.None, 0f);
        }
        internal static string GetString(ref byte[] data, int index, out int length)
        {
            List<byte> str = new List<byte>();
            while (data[index] != 0)
            {
                str.Add(data[index]);
                index++;
            }
            length = str.Count + 1;
            return System.Text.UTF8Encoding.UTF8.GetString(str.ToArray());
        }
    }
}
