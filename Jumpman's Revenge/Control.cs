﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace Jumpman_s_Revenge
{
    
    internal class Control
    {
        public Texture2D Texture;
        public Rectangle Position;
        public String Text;
        public String Name;
        public Control Parent;
        public Control(String name, Rectangle dest, Texture2D texture, String text)
        {
            this.Name = name;
            this.Position = dest;
            this.Texture = texture;
            this.Text = text;
        }
        public virtual void Update()
        {
        }
        public virtual void Draw()
        {
            if (Texture != null)
                Main.spriteBatch.Draw(Texture, Position, Color.White);
            if (Text != null || Text != "")
                Main.DrawString(Text, Position.Center.ToVector2(), 1f);
        }
    }
    internal class Button : Control
    {
        public byte State;
        private Rectangle[] sourceRects;
        public Rectangle Source { get { return this.sourceRects[State % 2]; } }

        public Action<Button> OnClick;

        public Button(String name, Rectangle dest, Texture2D texture, String text, Action<Button> onClick = null)
            : base(name, dest, texture, text)
        {
            this.OnClick = onClick;
            this.sourceRects = new Rectangle[2];
            this.sourceRects[0] = new Rectangle(0, 0, texture.Width, texture.Height / 2);
            this.sourceRects[1] = new Rectangle(0, texture.Height / 2, texture.Width, texture.Height / 2);
        }
        public override void Update()
        {
            MouseState mouseState = Mouse.GetState();
            if (mouseState.X >= Position.X && mouseState.X <= Position.Right && mouseState.Y >= Position.Y && mouseState.Y <= Position.Bottom)
            {
                if (mouseState.LeftButton == ButtonState.Pressed)                
                    State = 1;                
                else if (State == 1) // Click
                {
                    if (OnClick != null)
                        OnClick(this);
                    State = 0;
                }
            }
        }
        public override void Draw()
        {
            Main.spriteBatch.Draw(Texture, Position, Source, Color.White);
            if (Text != null && Text != "")
                Main.DrawString(Text, Position.Center.ToVector2(), Position.Height / 64f);
        }
    }
    internal class FrameBox : Control
    {
        public TileCollection Collection;
        public FrameBox(String name, Rectangle dest, TileCollection texture, String text)
            : base(name, dest, texture.texture, text)
        {
            this.Collection = texture;
        }
        public override void Draw()
        {
            bool topbottom = true;
            bool leftright = true;
            int w = Collection.tileW;
            int h = Collection.tileH;

            if (Position.Width <= h * 2)
            {
                w = Position.Width / 2;
                topbottom = false;
            }
            if (Position.Height <= h * 2)
            {
                h = Position.Height / 2;
                leftright = false;
            }
            Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, Position.Y, w, h), Collection.GetSourceRect((short)BorderTile.TopLeft), Color.White);
            Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, Position.Y, w, h), Collection.GetSourceRect((short)BorderTile.TopRight), Color.White);
            Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, Position.Bottom - h, w, h), Collection.GetSourceRect((short)BorderTile.BottomLeft), Color.White);
            Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, Position.Bottom - h, w, h), Collection.GetSourceRect((short)BorderTile.BottomRight), Color.White);
            if (topbottom)
            {
                Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.X + w, Position.Y, Position.Width - w * 2, h), Collection.GetSourceRect((short)BorderTile.Top), Color.White);
                Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.X + w, Position.Bottom - h, Position.Width - w * 2, h), Collection.GetSourceRect((short)BorderTile.Bottom), Color.White);
            }
            if (leftright)
            {
                Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.X, Position.Y + h, w, Position.Height - h * 2), Collection.GetSourceRect((short)BorderTile.Left), Color.White);
                Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.Right - w, Position.Y + h, w, Position.Height - h * 2), Collection.GetSourceRect((short)BorderTile.Right), Color.White);
            }
            if (topbottom && leftright)
                Main.spriteBatch.Draw(Collection.texture, new Rectangle(Position.X + w, Position.Y + h, Position.Width - w * 2, Position.Height - h * 2), Collection.GetSourceRect((short)BorderTile.Center), Color.White);


            if (Text != null && Text != "")
            {
                Vector2 testSize = Main.MainFont.MeasureString(Text);
                Main.DrawString(Text, Position.Center.ToVector2(), 1f);
            }
        }
    }

    internal class MessageBox : FrameBox
    {
        public Button CloseButton;
        Action<MessageBox> BoxClosing;
        public MessageBox(String name, Rectangle dest, TileCollection frame, Texture2D closeButton, String text, Action<MessageBox> OnClose)
            : base(name, dest, frame, text)
        {
          /*  Color[] colors = new Color[18];
            for (int i = 0; i < colors.Length; i++)
            {
                if (new int[] { 0,2,4,6,8,9,11,13,15,17 }.Contains(i))
                    colors[i] = Color.Black;
                else 
                    colors[i] = Color.White;
            }

            Texture2D cross = new Texture2D(Main.graphics.GraphicsDevice, 3,6);
            cross.SetData(colors);*/

            CloseButton = new Button("close", new Rectangle(dest.Right - 40, dest.Y + 8, 32, 32), closeButton, null, Close);
            BoxClosing = OnClose;
        }
        public override void Update()
        {
            CloseButton.Update();
            base.Update();
        }
        public override void Draw()
        {
            base.Draw();
            CloseButton.Draw();
        }
        void Close(Button b)
        {
            if (BoxClosing != null)
                BoxClosing(this);
        }
    }
    public enum BorderTile
    {
        TopLeft = 0,
        Top = 1,
        TopRight = 2,
        Left = 3,
        Center = 4,
        Right = 5,
        BottomLeft = 6,
        Bottom = 7,
        BottomRight = 8
    }


}
